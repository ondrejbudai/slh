<?php
namespace Slh\Utils;

class Autoloader {

    private static function getPath($classname){
        $components = explode('\\', $classname);

        $dirs = array_splice($components, 1, count($components) - 2);

        foreach($dirs as &$dir){
            $dir = strtolower($dir);
        }

        $path = DOCUMENT_ROOT . '/' . join('/', $dirs) . '/' . end($components) . '.php';

        return $path;
    }

    public static function start(){
        spl_autoload_register(function($classname){

            include Autoloader::getPath($classname);
        });
    }

    public static function exists($classname){
        return file_exists(Autoloader::getPath($classname));
    }
}
