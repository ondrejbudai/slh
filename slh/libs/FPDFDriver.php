<?php

require_once 'fpdf/fpdf.php';

class FPDFDriver extends FPDF {

    public function writeText($x, $y, $text, $style = '', $size = 10, $font = 'ariala') {
        $this->SetFont($font, $style, $size);
        $text = iconv('UTF-8', 'windows-1250//TRANSLIT', $text);
        $this->Text($x, $y, $text);
    }

    public function writeCenteredText($y, $text, $style = '', $size = 10, $font = 'ariala') {
        $this->SetFont($font, $style, $size);
        $text = iconv('UTF-8', 'windows-1250//TRANSLIT', $text);
        $width = $this->GetStringWidth($text);
        $this->Text(297.64 - $width / 2, $y, $text);
    }

    public function writeCell($x, $y, $w, $h, $text, $border = 0, $align = 'L', $style = '', $size = 10, $font = 'ariala') {
        $this->SetFont($font, $style, $size);
        $text = iconv('UTF-8', 'windows-1250//TRANSLIT', $text);
        $this->SetXY($x, $y);
        $this->Cell($w, $h, $text, $border, 0, $align);
        return $x + $w;
    }

}