<?php

namespace Slh\Utils;

class Config
{
    public static function getConfig()
    {
        return [
            'pass' => 'USER_PASSWORD',
            'dbHost' => 'DB_HOST',
            'dbUser' => 'DB_USER',
            'dbPassword' => 'DB_PASSWORD',
            'dbDatabase' => 'DB_DATABASE',
            'latestYear' => 0
        ];
    }
}
