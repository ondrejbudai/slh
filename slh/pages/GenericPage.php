<?php

namespace Slh\Pages;

abstract class GenericPage extends Page {
    
    public $requiresLogin = true;
    
    public function __construct($page){
            
        parent::__construct($page);
        $this->putFile('CONTENT', 'templates/layout.html');
    }
}
