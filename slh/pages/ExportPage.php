<?php

namespace Slh\Pages;

use Slh\Utils\Utils;

require_once DOCUMENT_ROOT . '/libs/FPDFDriver.php';

class ExportPage extends GenericPage {

    public $requiresDB = true;
    protected $title = 'Export výsledků';

    public function process() {
        $page = $this->page;
        switch ($page) {
            case 'export':
                $this->processExport();
                break;
            case 'do-export':
                $this->processDoExport();
                break;
        }
    }

    private function processExport() {
        global $m;
        $query = $m->query('SELECT COUNT(*) AS count, round FROM results WHERE time<>0 && year = ' . Utils::getYear() . ' GROUP BY round ORDER BY round ASC;');
        $maxround = 1;
        while ($data = $query->fetch_assoc()) {
            if ($data['count'] == 0)
                continue;
            $maxround = $data['round'];
        }
        $roundsChecked = array('', '', '', '', '', '');
        $roundsChecked[$maxround - 1] = ' checked';

        $this->putFile('CONTENT', 'templates/export.html');

        for ($i = 0; $i < 6; $i++) {
            $this->putVar("CHECKED[$i]", $roundsChecked[$i]);
        }
    }

    private function processDoExport() {
        if ($_POST['export-type'] == 'standard') {
            $this->doPDFExport();
        }
        if ($_POST['export-type'] == 'announcement') {
            $this->doAnnouncementPDFExport();
        }
    }

    private function initPDF() {
        $f = new \FPDFDriver('P', 'pt', 'A4');
        $f->AddFont('ariala');
        $f->AddFont('ariala', 'b');
        $f->SetAutoPageBreak(true);

        return $f;
    }

    private function doPDFExport() {
        $globalData = $this->getGlobalData();

        if (!isset($_POST['max-round']))
            $_POST['max-round'] = 6;

        $f = $this->f = $this->initPDF();


        for ($round = 1; $round < $_POST['max-round'] + 1; $round++) {
            $roundData = $this->getRoundData($round);
            $f->AddPage();
            $f->writeCenteredText(30, Utils::toUpper($globalData['name']), 'b', 20);

            $this->writeInfo(60, 'Název soutěže:', "{$globalData['name']} $round. kolo");
            $this->writeInfo(72, 'Číslo soutěže:', "{$roundData['competition_num']}");
            $this->writeInfo(84, 'Datum konání:', "{$roundData['date']}");
            $this->writeInfo(96, 'Místo konání:', "{$globalData['place']}");
            $this->writeInfo(108, 'Kategorie:', "{$globalData['category']}");
            $this->writeInfo(120, 'Pořadatel:', "{$globalData['organizer']}");
            $this->writeInfo(132, 'Ředitel soutěže:', "{$globalData['boss']}");
            $this->writeInfo(144, 'Jury:', "{$globalData['jury']}");
            $this->writeInfo(156, 'Počasí:', "{$roundData['weather']}");

            $f->writeCenteredText(182, "Výsledky {$round}. kolo", 'b', 12);

            $f->writeText(40, 202, 'Senioři:', 'ub');
            $curY = $this->writeRoundTable(40, 217, $this->getRoundCategoryData($round, 'senior'));
            $f->writeText(40, $curY + 20, 'Junioři:', 'ub');
            $curY = $this->writeRoundTable(40, $curY + 35, $this->getRoundCategoryData($round, 'junior'));
            $f->writeText(40, $curY + 20, 'Starší žáci:', 'ub');
            $curY = $this->writeRoundTable(40, $curY + 35, $this->getRoundCategoryData($round, 'starší žák'));
            $f->writeText(40, $curY + 20, 'Mladší žákyně:', 'ub');
            $curY = $this->writeRoundTable(40, $curY + 35, $this->getRoundCategoryData($round, 'mladší žákyně'));
            $f->writeText(40, $curY + 20, 'Mladší žáci:', 'ub');
            $curY = $this->writeRoundTable(40, $curY + 35, $this->getRoundCategoryData($round, 'mladší žák'));
        }

        $categoryData = $this->getCategoryData();

        $f->AddPage();
        $f->writeCenteredText(30, Utils::toUpper($globalData['name']), 'b', 20);
        $f->writeCenteredText(60, "Celkové výsledky", 'b', 16);
        $this->writeInfo(80, "Senioři:", $categoryData['senior']);
        $this->writeInfo(92, "Junioři:", $categoryData['junior']);
        $this->writeInfo(104, "Starší žáci:", $categoryData['starší žák']);
        $this->writeInfo(116, "Mladší žákyně:", $categoryData['mladší žákyně']);
        $this->writeInfo(128, "Mladší žáci:", $categoryData['mladší žák']);

        $this->writeOverallTable(30, 140, $this->getOverallData());

        $time = time();

        mkdir(DOCUMENT_ROOT . "/../www/exports/$time");
        chmod(DOCUMENT_ROOT . "/../www/exports/$time", 0757);

        $f->Output(DOCUMENT_ROOT . "/../www/exports/$time/slh.pdf");
        $f->Close();
        $this->putFile('CONTENT', 'templates/done-export.html');
        $this->putVar('TYPE', 'PDF');
        $this->putVar('PATH', "exports/$time/slh.pdf");
    }

    private function doAnnouncementPDFExport() {
        $f = $this->f = $this->initPDF();

        $globalData = $this->getGlobalData();

        $f->AddPage();
        $f->writeCenteredText(30, Utils::toUpper($globalData['name']), 'b', 20);

        $data = $this->getOverallData();

        //Overall:
        $f->writeText(40, 60, 'Absolutní pořadí', 'b', 16);
        $this->writeOverallTable(30, 80, $data, 3);

        //Senioři:
        $f->writeText(40, 180, 'Senioři', 'b', 16);
        $this->writeOverallTable(30, 200, $data, 3, 'senior');

        //Junioři:
        $f->writeText(40, 300, 'Junioři', 'b', 16);
        $this->writeOverallTable(30, 320, $data, 3, 'junior');

        //Starší žáci:
        $f->writeText(40, 420, 'Starší žáci', 'b', 16);
        $this->writeOverallTable(30, 440, $data, 3, 'starší žák');
        
        //Mladší žákyně:
        $f->writeText(40, 540, 'Mladší žákyně', 'b', 16);
        $this->writeOverallTable(30, 560, $data, 3, 'mladší žákyně');

        //Mladší žáci:
        $f->writeText(40, 660, 'Mladší žáci', 'b', 16);
        $this->writeOverallTable(30, 680, $data, 3, 'mladší žák');
        
        

        $time = time();
        mkdir("exports/$time");
        chmod("exports/$time", 0757);

        $f->Output("exports/$time/slh.pdf");
        $f->Close();
        $this->putFile('CONTENT', 'templates/done-export.html');
        $this->putVar('TYPE', 'PDF');
        $this->putVar('PATH', "exports/$time/slh.pdf");
    }

    public function writeInfo($y, $title, $text) {
        $f = $this->f;
        $f->writeText(40, $y, $title, 'b');
        $f->writeText(150, $y, $text);
    }

    public function writeRoundTable($x, $y, $data) {
        $f = $this->f;
        $i = 1;
        foreach ($data as $value) {
            if($y > 800){
                $y = 40;
                $f->AddPage();
            }
            $wx = $x;
            $wx = $f->writeCell($wx, $y, 20, 12, $i . '.', 1, 'R', 'b', 8);
            $wx = $f->writeCell($wx, $y, 100, 12, $value['name'], 1, 'L', 'b', 8);
            $wx = $f->writeCell($wx, $y, 80, 12, $value['club'], 1, 'L', '', 8);
            $wx = $f->writeCell($wx, $y, 50, 12, $value['license'], 1, 'L', '', 8);
            foreach ($value['times'] as $time) {
                $wx = $f->writeCell($wx, $y, 18, 12, $time, 1, 'R', '', 8);
            }
            $f->writeCell($wx, $y, 30, 12, $value['sum'], 1, 'R', 'b', 8);
            $y += 12;
            
            $i++;
        }

        return $y;
    }

    private function getRoundData($round) {
        global $m;
        $query = $m->query("SELECT date, competition_num, weather FROM rounds WHERE round=$round && year = " . Utils::getYear());

        return $query->fetch_assoc();
    }

    private function getRoundCategoryData($round, $category) {
        global $m;

        $ret = array();

        $query = $m->query("
            SELECT p.name, SUM(r.time) AS sum, p.id, p.license, p.club
            FROM participants p
            LEFT JOIN results r
            ON p.id = r.participant_id AND p.year = r.year
            WHERE (SELECT COUNT(*) FROM results re WHERE re.round = $round AND re.participant_id = p.id AND re.year = " . Utils::getYear() . ") <> 0 AND p.category = '$category' AND r.round=$round AND p.year = " . Utils::getYear() . "
            GROUP BY p.id ORDER BY SUM(r.time) DESC;");
        $i = 0;
        while ($data = $query->fetch_assoc()) {
            if ($data['sum'] == 0)
                continue;
            $ret[$i]['name'] = $data['name'];
            $ret[$i]['license'] = $data['license'];
            $ret[$i]['sum'] = $data['sum'];
            $ret[$i]['club'] = $data['club'];

            $query2 = $m->query("SELECT time FROM results WHERE round=$round AND participant_id = {$data['id']} AND year = " . Utils::getYear() . " ORDER BY attempt ASC");
            while ($times = $query2->fetch_assoc()) {
                $ret[$i]['times'][] = $times['time'];
            }
            $i++;
        }

        return $ret;
    }

    private function getGlobalData() {
        global $m;
        $query = $m->query('SELECT * FROM settings WHERE year = ' . Utils::getYear());
        return $query->fetch_assoc();
    }

    private function getCategoryData() {
        global $m;
        $query = $m->query('SELECT category, age FROM categories WHERE year = ' . Utils::getYear());
        while ($data = $query->fetch_assoc()) {
            $ret[$data['category']] = $data['age'];
        }

        return $ret;
    }

    private function getOverallData() {
        global $m;
        $query = $m->query('SELECT id, name, p.category, club, coef FROM participants p LEFT JOIN categories c ON p.category=c.category AND p.year = c.year WHERE p.year = ' . Utils::getYear());
        while ($data = $query->fetch_assoc()) {
            $query2 = $m->query("SELECT SUM(time) AS sum, round FROM results WHERE participant_id={$data['id']} AND year = " . Utils::getYear() . " GROUP BY participant_id, round ORDER BY SUM(time) DESC;");
            $i = 0;
            $tmpsum = 0;
            while ($tmp = $query2->fetch_assoc()) {
                if ($i < 3) {
                    $tmpsum += $tmp['sum'];
                }
                $data['results'][$tmp['round']] = $tmp['sum'];
                $i++;
            }
            $data['sum'] = round($tmpsum * $data['coef']);
            $ret[] = $data;
        }

        uasort($ret, function($a, $b){
            if ($a['sum'] == $b['sum']) {
                return 0;
            }
            return ($a['sum'] < $b['sum']) ? 1 : -1;
        });

        return $ret;
    }

    private function writeOverallTable($x, $y, $data, $maxNum = -1, $categoryFilter = '') {
        $i = 1;
        $f = $this->f;
        $wx = $x;
        $wx = $f->writeCell($wx, $y, 25, 16, 'Poř.', 1, 'L', 'b');
        $wx = $f->writeCell($wx, $y, 120, 16, 'Jméno', 1, 'L', 'b');
        $wx = $f->writeCell($wx, $y, 25, 16, 'Kat.', 1, 'L', 'b');
        $wx = $f->writeCell($wx, $y, 80, 16, 'Klub', 1, 'L', 'b');
        $wx = $f->writeCell($wx, $y, 24, 16, '1.', 1, 'C', 'b');
        $wx = $f->writeCell($wx, $y, 24, 16, '2.', 1, 'C', 'b');
        $wx = $f->writeCell($wx, $y, 24, 16, '3.', 1, 'C', 'b');
        $wx = $f->writeCell($wx, $y, 24, 16, '4.', 1, 'C', 'b');
        $wx = $f->writeCell($wx, $y, 24, 16, '5.', 1, 'C', 'b');
        $wx = $f->writeCell($wx, $y, 24, 16, '6.', 1, 'C', 'b');
        $wx = $f->writeCell($wx, $y, 32, 16, 'Koef.', 1, 'L', 'b');
        $wx = $f->writeCell($wx, $y, 50, 16, 'Celkem', 1, 'C', 'b');
        $y += 16;
        foreach ($data as $value) {
            if (empty($categoryFilter) == false && $value['category'] != $categoryFilter)
                continue;
            $wx = $x;
            $wx = $f->writeCell($wx, $y, 25, 16, "$i.", 1, 'R', 'b');
            $wx = $f->writeCell($wx, $y, 120, 16, $value['name'], 1, 'L', 'b');
            $wx = $f->writeCell($wx, $y, 25, 16, $this->getShorterCategory($value['category']), 1);
            $wx = $f->writeCell($wx, $y, 80, 16, $value['club'], 1);
            for ($j = 1; $j < 7; $j++) {
                if (isset($value['results'][$j]) == false)
                    $value['results'][$j] = 0;
                $wx = $f->writeCell($wx, $y, 24, 16, $value['results'][$j], 1, 'R');
            }

            $wx = $f->writeCell($wx, $y, 32, 16, $value['coef'], 1, 'R');
            $wx = $f->writeCell($wx, $y, 50, 16, $value['sum'], 1, 'R', 'b');
            $y += 16;
            if($y > 800){
                $y = 40;
                $f->AddPage();
            }
            $i++;
            if ($maxNum != -1 && $i > $maxNum)
                break;
        }
    }

    private function getShorterCategory($category) {
        static $categoryList = array(
    'mladší žák' => 'mž.',
    'starší žák' => 'sž.',
    'junior' => 'jun.',
    'senior' => 'se.',
    'mladší žákyně' => 'mžy.'
        );
        return $categoryList[$category];
    }

}
