<?php

namespace Slh\Pages;

abstract class Page {

    protected $buffer;
    public $requiresLogin = false;
    public $requiresDB = false;
    protected $page;
    protected $title = '';

    public function __construct($page) {
        ob_start();
        require DOCUMENT_ROOT . '/templates/page.html';
        $this->buffer = ob_get_contents();
        ob_end_clean();
        
        $this->page = $page;
        $this->setTitle($this->title);
    }

    public abstract function process();

    protected function putVar($place, $var) {
        $this->buffer = str_replace('$'. $place, $var, $this->buffer);
    }

    protected function putFile($place, $file) {
        ob_start();
        require DOCUMENT_ROOT . '/' . $file;
        $tmp = ob_get_contents();
        ob_end_clean();
        $this->putVar($place, $tmp);
    }

    protected function setTitle($title) {
        if (empty($title) == true) {
            $this->putVar('TITLE', 'SLH manažer');
        } else {
            $this->putVar('TITLE', $title . ' | SLH manažer');
        }
    }

    public function flush() {
        if($this->requiresDB == true){
            global $m;
            if($m->numQueries == 0){
                $this->putVar('NUMQUERIES', 'žádný - toto může značit neoptimální stav aplikace!');
            } else {
                $this->putVar('NUMQUERIES', $m->numQueries);
            }
            
        }
        else {
            $this->putVar('NUMQUERIES', 'žádný');
        }
        
        echo $this->buffer;
    }

}
