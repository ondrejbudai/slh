<?php

namespace Slh\Pages;

use Slh\Utils\Utils;

class HomePage extends GenericPage {

    public function __construct($p){
        parent::__construct($p);
        if($p == 'set-year'){
            $_SESSION['year'] = $_POST['year'];
        }
    }

    public function process(){
        $this->putFile('CONTENT', 'templates/homepage.html');
        $this->putVar('YEAR', Utils::getYear() + 2011);

        $options = "";
        for($i = 1; $i <= Utils::getLatestYear(); $i++){
            $year = 2011 + $i;
            $options .= <<<TAG
<option value="{$i}" SELECTED>{$year}</option>
TAG;
            $options = str_replace('SELECTED', (Utils::getYear() == $i) ? 'selected' : '', $options);

        }
        $this->putVar('OPTIONS', $options);
    }
}
