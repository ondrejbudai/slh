<?php

namespace Slh\Pages;

use Slh\Utils\Config;

class LoginPage extends Page {
    
    protected $title = 'Login';
    
    public function __construct($page) {
        if($page == 'logout'){
            $this->requiresLogin = true;
        }
        parent::__construct($page);
    }

    public function process() {
        $page = $this->page;

        switch ($page) {
            case 'login':
                $this->showBlankLoginPage();
                break;
            case 'do-login':
                $this->doLogin();
                break;
            case 'logout':
                $this->logOut();
                break;
            case 'need-login':
                $this->showErrorLoginPage('Na tuto akci se potřebujete přihlásit!');
                break;
        }
    }

    private function showLoginPage() {
        $this->putFile('CONTENT', 'templates/login.html');
    }

    private function showBlankLoginPage() {
        $this->showLoginPage();
        $this->putVar('NOTICE', '');
    }

    private function showErrorLoginPage($error) {
        $this->showLoginPage();
        $this->putVar('NOTICE', '<span class="error">' . $error . '</span><br /><br />');
    }

    private function showLogoutLoginPage($error) {
        $this->showLoginPage();
        $this->putVar('NOTICE', '<span class="logout">' . $error . '</span><br /><br />');
    }

    private function doLogin() {
        if (isset($_POST['pass']) == false) {
            $this->showLoginPage();
            return;
        }

        $conf = Config::getConfig();

        if ($_POST['pass'] != $conf['pass']) {
            $this->showErrorLoginPage('Špatné heslo!');
            return;
        }

        $_SESSION['pass'] = $_POST['pass'];
        header('Location: ?p=home');
    }

    private function logOut() {
        session_destroy();
        session_start();
        $this->showLogoutLoginPage('Byl jste úspěšně odhlášen!');
    }

}
