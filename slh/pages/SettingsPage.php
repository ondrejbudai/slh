<?php

namespace Slh\Pages;

use Slh\Utils\Utils;

class SettingsPage extends GenericPage {

    public $requiresDB = true;
    protected $title = 'Upravit nastavení';

    public function process() {
        $page = $this->page;
        switch ($page) {
            case 'edit-settings':
                $this->processEditSettings();
                break;
            case 'do-edit-settings':
                $this->processDoEditSettings();
                break;
        }
    }
    private function processDoEditSettings() {
        global $m;

        $name = $m->real_escape_string($_POST['name']);
        $place = $m->real_escape_string($_POST['place']);
        $category = $m->real_escape_string($_POST['category']);
        $organizer = $m->real_escape_string($_POST['organizer']);
        $boss = $m->real_escape_string($_POST['boss']);
        $jury = $m->real_escape_string($_POST['jury']);

        $m->query("REPLACE INTO settings VALUES (".  Utils::getYear().", '$name', '$place', '$category', '$organizer', '$boss', '$jury')");

        $this->processEditSettings();

    }

    private function processEditSettings() {
        $this->putFile('CONTENT', 'templates/edit-settings.html');

        global $m;
        $query = $m->query('SELECT * FROM settings WHERE year = ' . Utils::getYear());
        $data = $query->fetch_assoc();
        if($data == NULL){
            $year = Utils::getYear();
            $prevYear = $year - 1;
            $m->query("INSERT INTO settings (year, name, place, category, organizer, boss, jury)
                        SELECT {$year}, name, place, category, organizer, boss, jury FROM settings WHERE year = {$prevYear}");

            $query = $m->query('SELECT * FROM settings WHERE year = ' . Utils::getYear());
            $data = $query->fetch_assoc();
        }
        $keys = ['name', 'place', 'category', 'organizer', 'boss', 'jury'];

        foreach ($keys as $key) {
            $fieldName = strtoupper($key);
            if ($data === NULL || !array_key_exists($key, $data)) {
                $this->putVar($fieldName, '');
                continue;
            }

            $this->putVar($fieldName, htmlspecialchars($data[$key]));
        }
    }

}
