<?php

namespace Slh\Pages;

use Slh\Utils\Utils;

class RoundPage extends GenericPage {

    public $requiresDB = true;
    protected $title = 'Upravit kolo';

    public function process() {
        $page = $this->page;
        switch ($page) {
            case 'edit-round':
                $this->processEdit();
                break;
            case 'do-edit-round':
                $this->processDoEdit();
                break;
        }
    }

    private function processDoEdit() {
        global $m;
        $query = 'REPLACE INTO results (participant_id, year, round, attempt, time) VALUES ';
        $round = $m->real_escape_string($_GET['n']);

        foreach ($_POST['time'] as $id => $row) {
            $id = $m->real_escape_string($_POST['id'][$id]);

            foreach ($row as $attempt => $time) {
                if (is_numeric($time) == false)
                    $time = 0;
                if ($time > 60)
                    $time = 60;

                $attempt = $m->real_escape_string($attempt);
                $time = $m->real_escape_string($time);
                $query .= "($id, " . Utils::getYear() . ", $round, $attempt, $time),";
            }
        }

        $query = substr($query, 0, strlen($query) - 1);
        $query .= ';';

        $m->query($query);

        $date = $m->real_escape_string($_POST['date']);
        $competitionNum = $m->real_escape_string($_POST['competition_num']);
        $weather = $m->real_escape_string($_POST['weather']);

        $m->query("REPLACE INTO rounds (round, year, date, competition_num, weather) VALUES ($round, " . Utils::getYear() . ", '$date', '$competitionNum','$weather');");

        $this->processEdit();
    }

    private function processEdit() {
        global $m;
        $round = $m->real_escape_string($_GET['n']);
        $q = $m->query('SELECT id, name FROM participants WHERE year=' . Utils::getYear() . ' ORDER BY name ASC');

        $tmp = '';
        $id = 0;

        while ($data = $q->fetch_assoc()) {
            $query = $m->query("SELECT attempt, time FROM results
            WHERE round = $round && participant_id = {$data['id']} && year = " . Utils::getYear() . "
            ORDER BY attempt ASC;");

            if ($query->num_rows == 0) {
                //generate blank row
                $tmp .= "<tr><td>{$data['name']}</td>";
                for ($i = 0; $i < 10; $i++) {
                    $tmp .= '<td><input type="text" size="2" name="time[' . $id . '][' . $i . ']" value="0"></td>';
                }
                $tmp .= '<td>0</td></tr>';
            } else {
                $tmp .= "<tr><td>{$data['name']}</td>";
                $sum = 0;
                $i = 0;
                while ($data2 = $query->fetch_assoc()) {
                    $sum += $data2['time'];
                    $tmp .= '<td><input type="text" size="2" name="time[' . $id . '][' . $i . ']" value="' . $data2['time'] . '"></td>';
                    $i++;
                }
                $tmp .= "<td>$sum</td></tr>";
            }
            $tmp .= '<input type="hidden" name="id[' . $id . ']" value="' . $data['id'] . '">';
            $id++;
        }

        $query = $m->query("SELECT date, competition_num, weather FROM rounds WHERE round=$round && year = " . Utils::getYear());
        if ($query->num_rows == 1) {
            $result = $query->fetch_assoc();
        } else {
            $result['date'] = '';
            $result['competition_num'] = '';
            $result['weather'] = '';
        }

        $this->putFile('CONTENT', 'templates/edit-round.html');

        $this->putVar('DATE', htmlspecialchars($result['date']));
        $this->putVar('COMPETITION_NUM', htmlspecialchars($result['competition_num']));
        $this->putVar('WEATHER', htmlspecialchars($result['weather']));

        $this->putVar('TABLE', $tmp);
        $this->putVar('ROUND', htmlspecialchars($_GET['n']));
        $this->putVar('MAXID', $id);
    }

}
