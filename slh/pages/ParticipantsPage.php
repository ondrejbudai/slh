<?php

namespace Slh\Pages;

use Slh\Utils\Utils;

class ParticipantsPage extends GenericPage {

    public $requiresDB = true;
    protected $title = 'Upravit závodníky';

    public function process() {
        $page = $this->page;
        switch ($page) {
            case 'edit-participants':
                $this->processEdit();
                break;
            case 'do-edit-participants':
                $this->processDoEdit();
                break;
            case 'do-add-participants':
                $this->processDoAdd();
                break;
            case 'do-upgrade-participants':
                $this->processDoUpgrade();
                break;
        }
    }

    private function processEdit() {
        $this->putFile('CONTENT', 'templates/edit-participants.html');
        global $m;
        $query = $m->query('SELECT id, name, category, club, license FROM participants WHERE year=' . Utils::getYear() . ' ORDER BY name ASC;');
        $tmp = '';
        $tmp2 = '';
        $tmp3 = '';
        $i = 0;
        while ($data = $query->fetch_assoc()) {
            $tmp .= "<tr id=\"row" . $i . "\"><td id=\"name" . $i . "\">{$data['name']}</td><td id=\"category" . $i . "\">{$data['category']}</td><td id=\"club" . $i . "\">{$data['club']}</td><td id=\"license" . $i . "\">{$data['license']}</td><td><a href=\"#edit\" onClick=\"editRow(" . $i . ");\">upravit</a></td><td><a href=\"#delete\" onClick=\"delRow(" . $i . ");\">smazat</a></td><input type=\"hidden\" name=\"del{$i}\" value=\"0\"></tr>";
            $tmp2 .= 'new Array(' . $data['id'] . ',"' . $data['name'] . '","' . $data['category'] . '","' . $data['club'] . '","' . $data['license'] . '", 0),';
            $i++;
        }

        $query = $m->query('SELECT id, name, category, club FROM participants p WHERE p.year < ' . Utils::getYear() . ' AND (SELECT COUNT(*) FROM participants pa WHERE pa.id = p.id AND pa.year = ' . Utils::getYear() . ') = 0 AND (SELECT MAX(pb.year) FROM participants pb where p.name = pb.name) = p.year ORDER BY p.name ASC');
        while ($data = $query->fetch_assoc()) {
            $tmp3 .= "<tr><td>{$data['name']}</td><td>{$data['category']}</td><td>" . (($data['category'] != 'senior') ? '<input type="checkbox" value="ok" name="moveup[' . $data['id'] . ']" />' : '') . "</td><td><input type=\"checkbox\"  value=\"ok\" name=\"upgrade[{$data['id']}]\"/></td></tr>\n";
        }

        $tmp2 = substr($tmp2, 0, strlen($tmp2) - 1);
        $this->putVar('TABLE', $tmp);
        $this->putVar('DATA', $tmp2);
        $this->putVar('UPGRADE', $tmp3);
    }

    private function processData() {
        $tmp = array();
        if (isset($_POST['name']))
            foreach ($_POST['name'] as $index => $data) {
                $tmp[$index]['name'] = $data;
            }
        if (isset($_POST['category']))
            foreach ($_POST['category'] as $index => $data) {
                $tmp[$index]['category'] = $data;
            }
        if (isset($_POST['club']))
            foreach ($_POST['club'] as $index => $data) {
                $tmp[$index]['club'] = $data;
            }
        if (isset($_POST['license']))
            foreach ($_POST['license'] as $index => $data) {
                $tmp[$index]['license'] = $data;
            }
        foreach ($_POST['del'] as $index => $data) {
            $tmp[$index]['del'] = $data;
        }
        foreach ($_POST['id'] as $index => $data) {
            $tmp[$index]['id'] = $data;
        }

        return $tmp;
    }

    private function processDoEdit() {
        global $m;
        $data = $this->processData();
        foreach ($data as $value) {
            if ($value['del'] == 1) {
                $m->query("DELETE FROM participants WHERE id={$value['id']} AND year=" . Utils::getYear());
                $m->query("DELETE FROM results WHERE participant_id={$value['id']} AND year=" . Utils::getYear());
            } else {
                if (!isset($value['name']))
                    continue;
                $name = $m->real_escape_string($value['name']);
                $category = $m->real_escape_string($value['category']);
                $club = $m->real_escape_string($value['club']);
                $license = $m->real_escape_string($value['license']);
                $id = $m->real_escape_string($value['id']);
                $m->query("UPDATE participants SET name='{$name}',category='{$category}',club='{$club}',license='{$license}' WHERE id={$id} AND year=" . Utils::getYear());
            }
        }

        $this->processEdit();
    }

    private function processAddData() {
        $tmp = array();
        if (isset($_POST['name']))
            foreach ($_POST['name'] as $index => $data) {
                $tmp[$index]['name'] = $data;
            }
        if (isset($_POST['category']))
            foreach ($_POST['category'] as $index => $data) {
                $tmp[$index]['category'] = $data;
            }
        if (isset($_POST['club']))
            foreach ($_POST['club'] as $index => $data) {
                $tmp[$index]['club'] = $data;
            }

        return $tmp;
    }

    private function processDoAdd() {
        global $m;
        $data = $this->processAddData();

        foreach ($data as $value) {
            if (empty($value['name']) == true)
                continue;
            $name = $m->real_escape_string($value['name']);
            $category = $m->real_escape_string($value['category']);
            $club = $m->real_escape_string($value['club']);
            $m->query("INSERT INTO participants (year, name, category, club) VALUES (" . Utils::getYear() . ",'$name','$category','$club');");
        }

        $this->processEdit();
    }

    private function processDoUpgrade() {
        global $m;
        $data = '';
        foreach ($_POST['upgrade'] as $key => $value) {
            $data .= $key . ',';
        }
        if ($data != '') {
            $data = substr($data, 0, strlen($data) - 1);
            $query = "INSERT INTO participants (SELECT id, " . Utils::getYear() . ", license, name, category, club FROM participants p WHERE id IN ({$data}) AND year = (SELECT max(year) from participants pa where pa.name = p.name))";
            $m->query($query);
        }

        if(!empty($_POST['moveup'])) {
            foreach ($_POST['moveup'] as $key => $value) {
                $result = $m->query("SELECT category FROM participants WHERE id = $key AND year = " . Utils::getYear());
                $result = $result->fetch_assoc();
                switch ($result['category']) {
                    case 'mladší žák':
                    case 'mladší žákyně':
                        $new = 'starší žák';
                        break;
                    case 'starší žák':
                        $new = 'junior';
                        break;
                    case 'junior':
                        $new = 'senior';
                        break;
                }

                $m->query("UPDATE participants SET category='$new' WHERE id = $key AND year = " . Utils::getYear());
            }
        }
        $this->processEdit();
    }

}
