<?php
namespace Slh\Utils;

class MysqliDriver extends \Mysqli {
    public $numQueries = 0;

    public function __construct($conf){
        @parent::__construct($conf['dbHost'], $conf['dbUser'], $conf['dbPassword'], $conf['dbDatabase']);
        if(empty($this->connect_error) == false){
            echo "Database connection failed:<br />{$this->connect_error}";
            exit;
        }
        parent::query('SET NAMES UTF8');

    }

    public function query($data, $resultmode = MYSQLI_STORE_RESULT){
        $this->numQueries++;
        $ret = parent::query($data, $resultmode);
        if($ret == false) {
            echo "Database query failed:<br />$data<br />{$this->error}";
            exit;
        }
        return $ret;
    }
}
