<?php
namespace Slh\Utils;

class Utils {
    public static function loggedIn() {
    if (empty($_SESSION['pass'])) {
        return false;
    }

    $conf = Config::getConfig();

    return ($_SESSION['pass'] == $conf['pass']);
}

    public static function toUpper($string) {
        return (strtoupper(strtr($string, 'ěščřžýáíéúůďťň', 'ĚŠČŘŽÝÁÍÉÚŮĎŤŇ')));
    }

    public static function getLatestYear(){
        return Config::getConfig()['latestYear'];
    }

    public static function getYear() {
        if (!isset($_SESSION['year'])) {
            $_SESSION['year'] = self::getLatestYear();
        }
        return $_SESSION['year'];
    }

}
