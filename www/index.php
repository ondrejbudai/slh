<?php

use Slh\Utils\Utils;

set_error_handler(function($errno, $errstr, $errfile, $errline){
    echo '<pre>';
    debug_print_backtrace();
    echo '</pre>';
    exit($errno);
}, E_ALL & ~E_DEPRECATED);

define('PATH_TO_APP', '../slh');
define('DOCUMENT_ROOT', preg_replace('/\\/[^\\/]*$/', '/' . PATH_TO_APP, $_SERVER['SCRIPT_FILENAME']));
define('BASE_URL', preg_replace('/\\/[^\\/]*$/', '', $_SERVER['SCRIPT_NAME']));

session_start();
ini_set('default_charset', 'UTF-8');

require_once DOCUMENT_ROOT . '/autoloader.php';
require_once DOCUMENT_ROOT . '/inc/config.php';
\Slh\Utils\Autoloader::start();

if (isset($_GET['p']) == false)
    $page = '';
else
    $page = $_GET['p'];

switch ($page) {
    case 'login':
    case 'do-login':
    case 'logout':
        $p = new \Slh\Pages\LoginPage($page);
        break;
    case 'edit-participants':
    case 'do-edit-participants':
    case 'do-add-participants':
    case 'do-upgrade-participants':
        $p = new Slh\Pages\ParticipantsPage($page);
        break;
    case 'edit-round':
    case 'do-edit-round':
        $p = new Slh\Pages\RoundPage($page);
        break;
    case 'edit-settings':
    case 'do-edit-settings':
        $p = new Slh\Pages\SettingsPage($page);
        break;
    case 'export':
    case 'do-export':
        $p = new Slh\Pages\ExportPage($page);
        break;
    case 'home':
    case 'set-year':
        $p = new Slh\Pages\HomePage($page);
        break;
    default:
        if (Utils::loggedIn() == true) {
            $page = 'home';
            $p = new Slh\Pages\HomePage($page);
        } else {
            $page = 'login';
            $p = new Slh\Pages\LoginPage($page);
        }
        break;
}



if ($p->requiresLogin == true && Utils::loggedIn() == false) {
    unset($p);
    $page = 'need-login';
    $p = new Slh\Pages\LoginPage($page);
}


if ($p->requiresDB == true) {
    $m = new Slh\Utils\MysqliDriver(\Slh\Utils\Config::getConfig());
}



$p->process();

$p->flush();


if (isset($m) == true)
    $m->close();
